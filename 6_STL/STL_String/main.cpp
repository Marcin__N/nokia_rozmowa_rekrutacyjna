#include <iostream>
#include <string>			// biblioteka dla roznych moteod klasy string

using namespace std;

int main()
{
	{
		///////////////////////////////////////////////////////////////////////////////////////
		// WYBRANE KONSTRUKTORY
		string one("Pierwszy string utworzony przez cudzyslow.");		// inicjuje lancuchem znakow
		cout << one << endl;
		string two(20, '$');											// inicjuje 20 znakami $
		cout << two << endl;
		string three(one);												// konstruktor kopiujacy (gleboko)
		cout << three << endl;
		string four = three + two;										// konstruktor dzialajcy przez przeciazenie znaku przypisania i dodawania
		cout << four << endl;
		char alls[] = "Wszystko dobre, co od chara sie zaczyna";
		string five(alls, 20);											// konstruktor kopiujacy do okreslonej maksymalnej ilosci znakow
		cout << five << endl;
		string six(alls + 9, alls + 14);								// konstruktor kopiujacy OD-DO za pomoca adresow miejsc
		string seven(&five[9], &five[14]);								// j.w. inaczej odwolujac sie
		cout << six << ", " << seven << "..." << endl;
		string eight(four, 20, 9);										// inicjalizuja kopia od 20 znaku, 9 znakow
		cout << eight << "w akcji!" << endl;
	}
	cin.get();
	system("cls");


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							// ZASTOSOWANIE NIEKTORYCH PRZECIAZEN
	{
		string one = "Mozna dodawac stringi ";
		string two = "oraz do innych stringow.";
		string three = one + "do lancuchow znakowych " + two;
		cout << "One:\t" << one << endl;
		cout << "Two:\t" << two << endl;
		cout << "Three:\t" << three << endl;
		string four = "A nawet uzywac w srodku dodawania konstruktorow string, np string(5, '!')" + string(5, '!');
		cout << "Four:\t" << four << endl;
		string five = one;
		cout << endl << "Aktualnie five == one, zobaczmy czy operator porownania powie to samo: " << (five == one) << endl;
		cout << "Ale five to nie to samo co two, racja? ==: " << (five == two) << endl;
		cout << endl << "A operotory porownania, ktory jest \"wiekszy\"? Ten zostanie wypisany THREE vs TWO" << endl << endl;
		if (three >= two)
			cout << three;
		else if (two >= three)
			cout << two;					// dzieje sie tak nie z powodu ilosci liter a ich kolejnosci tablicy ASCII
	}
	cin.get();
	system("cls");


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							// PRZYDATNE METODY KLASY STRING

	{
		string one = "Bardzo dlugi string.";
		string two = "Krotki string.";
		cout << "Dlugosc one / two: " << one.length() << " / " << two.size() << endl;			// size i length zwracaja ilosc znakow
		string three = "Ten string zawiera KACZKE oraz inne slowa.";
		string four = "A ten posiada tylko PSA.";
		cout << endl <<"Funkcja find() zwraca indeks pierwszego znaku szukanego ciagu, jezeli wystapi." << endl;
		cout << "Poszukajmy KACZKE w three / four: " << three.find("KACZKE") << " / " << four.find("KACZKE") << endl;
		cout << "Jak widac four nie posiada KACZKE i wyrzucil dziwne liczby, tak naprawde zwraca wartosc string::npos, ale tutaj jej nie wychwycilismy.";
	}
	/*
	Klasa string posiada jeszcze wiel innych ciekawych metod podobnych do powyzszych, np: rfind(), find_first_of(), find_last_of(),
	find_first_not_of(), find_last_not_of().
	capacity()		Zwraca aktualna pojemnosc stringa
	strcat(...)		to camo co a += b
	strcpy(...)		to samo co a = b
	empty()			Zwraca warto�� true je�eli napis jest pusty.
	at()			Zwraca znak o podanym po�o�eniu, tak jak operator [], z tym �e ta metoda jest bezpieczniejsza - wyrzuca wyj�tek w przypadku wyj�cia poza zakres stringa.
	clear()			Usuwa wszystkie znaki z napisu.
	erase(...)		Usuwa wybrane znaki.
	find(...)		Znajduje podci�g w ci�gu, s� te� bardziej rozbudowane funkcje tego typu.
	swap(...)		Zamienia miejscami dwa stringi, a staje si� b, a b staje si� a.
	substr(...)		Zwraca podci�g na podstawie indeksu pocz�tkowego i d�ugo�ci podci�gu.
	append(...)		Dodaje zadany napis na ko�cu istniej�cego ci�gu.
	c_str()			Zwraca napis w stylu j�zyka C (sta�y wska�nik typu const char*).
	*/

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cin.get();
	return 0;
}