#include <iostream>
#include <vector>
using namespace std;
int main()
{
	vector<int> tab;

	tab.push_back(1);
	tab.push_back(2);
	tab.push_back(3);

	vector<int>::iterator it;			// tworzenie iteratora dla vectora intow
	for (it = tab.begin();				// ustawienia iteratora na poczatek
		it != tab.end();				// sprawdzenie czy iterator nie wskazuje na koniec
		++it)							// iteratowanie wprzod
	{
		cout << *it << '\n';
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////
	cout << endl << "Inny sposob z C++11: " << endl;
	for (auto &it : tab)				// nowy sposob petli for znany z C++11
	{
		cout << it << endl;
	}

	cin.get();
	return 0;
}