Operatory ponizej wymienione pozwalaja lepiej kontrolowac proces rzutowania:

1. DYNAMIC_CAST

Majac dwie klasy Father and Son. (Son dziedziczy po Father), oraz odpowiednio wska�niki pF i pS wyrazenie:
   pF = dynamic_cast<Son *> pS;
przypisuje wskaznikowi Son * do pF tylko wtedy kiedy klasa bazowa to Father, w innym wypadku przypisuje pusty wskaznik
Sluzy ten operator do rzutowania w gore wewnatrz hierarchii klas, ktore jest bezpieczne dzieki relacji jest-czyms, a takze uniemozliwienia innego formatowania.
Przyklady uzycia patrz main.cpp

2. CONST_CAST

Rzutowanie const_cast sluzy do zmiany etykiety pomiedy const oraz volatile. Je�li rzutowaniu podlega inny aspekt to wynikiem rzutowanie jest b��d.
Przyk�ad:
   Son obj_son;
   const Son * pcS;
   ...
   Son * pnF = const_cast<Son *> (pcS);
Rzutowanie pozwala uzyc wskaznika pnF do zmiany wartosci obiektu obj_son. Rzutowanie uzyteczne iedy wartosc powinna bys zazwyczaj stala, lecz w kilku miejscach chcemy cos w niej zmienic.
Trzeba pamietac ze przy rzutowaniu nie mozna zmienic innych rzeczy, np zamiast Son dac Father. ORaz rzutowanie to nie umozliwi nam edytowanie obiektu obj_son gdyby byl on const.

3. STATIC_CAST

Uzywane do jawnego rzutowania w gore/dol obiektow oraz jawnej zmiany liczby calkowitej na wartosc wyliczeniowa (np. float na int).

4. REINTERPRET_CAST

Uzywac bardzo ostroznie!
Sluzy do rzutowania potrzebnego do operacji bardzo niskopoziomowych. Mozna np rzutowac wskaznik na typ calkowitoliczbowy (jezeli go pomiesci).