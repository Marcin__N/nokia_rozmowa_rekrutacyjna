#include <iostream>
#include <cstdlib>
#include <ctime>
#include <typeinfo>			// do obslugi typeid i klasy type_info

/*
RTTI - mechanizm dynamicznej identyfikacji typu:
a) operator dynamic_cast tworzy wska�nik typu pochodnej ze wska�nika typu klasy bazowej, jesli konwersja niemozwlia
   to zwraca wskaznik pusty
b) operator typeid zwraca wartosc okreslajaca typ danego obiektu
c) struktura type_info przechowuje informacje o danym typie

*/

using namespace std;

class Grand
{
	int hold;
public:
	Grand(int h = 0) : hold(h) {}
	virtual void Speak() const { cout << "Jestem klasa Grand!" << endl; }
	virtual int Value() const { return hold; }
};

class Superb : public Grand
{
public:
	Superb(int h = 0) : Grand(h) {}
	virtual void Speak() const { cout << "Jestem klasa Superb!!" << endl; }
	virtual void Say() const { cout << "Przechowuje wartosc klasy Superb, ktora wynosi " << Value() << "!" << endl; }
};

class Magnificent : public Superb
{
	char ch;
public:
	Magnificent(int h = 0, char c = 'A') : Superb(h), ch(c) {}
	virtual void Speak() const { cout << "Jestem klasa Magnificent!!!" << endl; }
	virtual void Say() const { cout << "Przechowuje znak '" << ch << "' oraz liczbe" << Value() << "!" << endl; }
};

Grand * GetOne()
{
	Grand * p = nullptr;
	switch (rand() % 3)
	{
	case 0: p = new Grand(rand() % 100);
		break;
	case 1: p = new Superb(rand() % 100);
		break;
	case 2: p = new Magnificent(rand() % 100, 'A' + rand()%26);
		break;
	}
	return p;
}

int main()
{
	srand(time(0));
	Grand * pg;
	Superb * ps;
	for (int i = 0; i < 5; i++)
	{
		pg = GetOne();
		cout << "Teraz przetwarzab obiekt typu " << typeid(*pg).name() << endl;			// typeid zwraca referencje do obiektu type_info ktory
																						// posiada metode zwracajacej (zazwyczaj) rodzaj obiektu
		pg->Speak();
		if (ps = dynamic_cast<Superb *>(pg))							// dynamic_cast rzutuje, gdy ma zagwarantowane ze to bezpieczne, inaczej
			ps->Say();													// zwraca pusty wskaznik
		if (typeid(Magnificent) == typeid(*pg))						//typeid sprawdza czy wskaznik *pg jest typu Magnificent
			cout << "Tak, rzeczywiscie jestes wspaniala." << endl;
		cout << endl;
	}

	cin.get();
	return 0;
}