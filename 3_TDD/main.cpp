#include <iostream>
#include <gtest\gtest.h>
struct BankAccount
{
	int balance;

	BankAccount() {}

	explicit BankAccount(const int balance) :balance{ balance } {}
};

TEST(AccountTest, BankAccountStartsEmpty)
{
	BankAccount account;
	EXPECT_EQ(0, account.balance);
}

int main(int argc, char* argv[])
{
	std::cin.get();

	//testing::InitGoogleTest(&argc, argv);
	return 0; //RUN_ALL_TESTS();
}