#include <cmath>
#include "exception_by_object.h"

/*
1. Program wywoluje funkcje wewnatrz bolku try.
2. Funkcja zglasza wyjatek.
3. Wychwytywanie wyjatkow w bloku catch.
ewentualnie:
4. Rozwiniecie stosu / zakonczenie programu.
*/


double hmean_nonobject(double a, double b);
double gmean(double a, double b);
double hmean(double a, double b);

int main()
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
	double x, y, z;
	cout << "Podaj dwie liczby: ";
	while (cin >> x >> y)
	{
		try
		{
			z = hmean_nonobject(x, y);
		}
		catch (const char * s)
		{
			cout << s << endl;
			cout << "Podaj kolejna pare liczb: ";
			continue;
		}
		cout << "Srednia harmoniczna liczb " << x << " i " << y << " wynosi\t" << z << endl;
		cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
	}
	cout << "Koniec programu";

*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double x, y, z;
	cout << "Podaj dwie liczby: ";
	while (cin >> x >> y)
	{
		try
		{
			cout << "Srednia harmoniczna liczb " << x << " i " << y << " wynosi\t" << hmean(x, y) << endl;
			cout << "Srednia geometryczna liczb " << x << " i " << y << " wynosi\t" << gmean(x, y) << endl;
			cout << "Podaj kolejna pare liczb <w, aby wyjsc>: ";
		}
		catch (bad_hmean & bg)
		{
			bg.mesg();
			cout << "Sprobouj ponownie." << endl;
			continue;
		}
		catch (bad_gmean & hg)
		{
			cout << hg.mesg();
			cout << "Uzyte wartosi: " << hg.v1 << ", " << hg.v2 << endl;
			cout << "Niestety to koniec zabawy!" << endl;
			abort();		// <- konczy program natychmiastowo, mozna uzyc tez break by wyjsc z petli
		}
	}
	cout << "Koniec programu";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	cin.get();
	return 0;
}

double hmean_nonobject(double a, double b)
{
	if (a == -b)
		throw "Niepoprawne argumenty funkcji hmean(): a = -b nie jest dozwolone";
	return 2.0 * a * b / (a + b);
}

double gmean(double a, double b)
{
	if (a < 0 || b < 0)
		throw bad_gmean(a, b);
	return sqrt(a * b);
}

double hmean(double a, double b)
{
	if (a == -b)
		throw bad_hmean(a, b);
	return 2.0 * a * b / (a + b);
}