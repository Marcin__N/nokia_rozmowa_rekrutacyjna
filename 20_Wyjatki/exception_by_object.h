#pragma once
#include <iostream>
/*
Klasy zajmujace sie wyjatkami dla odpowiednich funkcji
*/

using namespace std;

class bad_hmean
{
	double v1;
	double v2;

public:
	bad_hmean(double a = 0, double b = 0) :v1(a), v2(b)
	{ 	}

	void mesg()
	{
		cout << "hmean(" << v1 << ", " << v2 << "): niepoprawne argumenty: a = -b" << endl;
	}
};

class bad_gmean
{
public:

	double v1;
	double v2;
	bad_gmean(double a = 0, double b = 0) :v1(a), v2(b){}

	const char * mesg()
	{
		return "Argumenty funkcji gmean() powinny byc >= 0\n";
	}
};