#include <iostream>
#include <string>

using namespace std;

// stala symboliczna -  zasieg globalny, KWALIFIKATOR, deklaruje stala, obowiazkowa natychmiastowa deklaracja
const int MONTHS = 12;

class Banknot
{
public:					// dla ograniczenia pisania seterow i geterow wszystkie zmienna sa publiczne

	string name;
	int ID;
	int value;
	const string country = "Poland";		// stala wartosc/nazwa dla kazdego obiektu tworzonego w tej funkcji, inny sposob deklaracji
											// inny sposob deklaracji szukaj w 16. lista inicjalizacyjna


	const string get_country() const		// const przed string oznacza, ze wartosc zwracana bedzie stala (r-wartoscia)
	{										// const za definicja funkcji oznacza, ze nie bedzie ona modyfikowala zmiennych obiektu, na rzecz
		return country;						// ktorego zostala wywolana
	}

	int how_many_per_month(const int month)	// const przed rodzajem zmiennej oznacza, ze nie bedzie ona modyfikowana w tej funkcji
	{
		return value / month;
	}

	const Banknot * which_cheaper(const Banknot * BK)	// wskaznik na stala, oznacza ze obiekt BK nie moze byc modyfikowany
	{
		//BK->value++;			// niedozwolone, poniewaz staly obiekt
		if (this->value > BK->value)
			return BK;			// niedozwolone gdyby tpy zwracany byl bez const
		if (this->value < BK->value)
			return this;
		else
			return nullptr;
	}
	
	Banknot * which_name_longer(Banknot * const BK)	// staly wskaznik, nie mozna zmieniac na co wksazuje
	{
		if ((this->name).length() > (BK->name).length())
		{
			//BK = this;		// niedozwolone do wskaznik BK jest staly, wiec nie mozna zmienic na co wskazuje
			return BK;			// natomiast mozna zwrocic ten wskaznik nawet jezeli typ zwracany funkcji nie jest const
		}
		if ((this->name).length() < (BK->name).length())
			return this;
		else
			return nullptr;
	}
};


int main()
{
	Banknot B;		// trzeba zrobic konstruktory by testowac klase

	cout << "Ilosc miesiecy w roku: " << MONTHS << endl;					// poprawne uzywanie stalej symbolicznej
	//cout << "Ilosc miesiecy w roku bez grudnia: " << --MONTHS << endl;	// niedozwolone, poniewaz nie mozna jej modyfikowac
	cout << "Ilosc miesiecy w roku bez grudnia: " << MONTHS-1 << endl;		// natomiast mozna uzywaj ja w dzialaniach


	cin.get();
	return 0;
}