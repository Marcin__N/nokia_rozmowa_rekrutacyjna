////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	REFERENCJA VS WSKAZNIK ORAZ ZMIENNE TYMCZASOWE
Zmienne tymczasowe (przy referencji):
Jesli parametr referencyjny jeststaly kompilator generuje zmienna tymczasowa w dwojakich sytuacjach:
- kiedy parametr wywolania ma prawidlowy typ, ale nie jest l-wartoscia
- kiedy parametr wywolywania ma niewlasciwy typ, ale ten typ mozna przekonwertowac na wlasciwy

	CZYM SIE ROZNI WSKAZNIK OD REFERENCJI
A. FUNKCJE
   1. Sposob deklaracji parametrow funkcji.
   2. Wersja wskaznikowa wymaga uzycia operatora dereferencji.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	DYNAMICZNE ALOKOWANIE
Przy dynamicznym alokowaniu, np.:
   int * ptr1 = new int;
   int * ptr2 = new int[il_el];
Trzeba pamietac o zwolnieniu pamieci, bo inaczej mozemy miec wycieki pamieci, zwalniamy odpowiednio:
   delete ptr1;
   delete [] ptr2;

Nie zwalniajac pamieci bedziemy mieli wycieki pamieci, ktore z czasem dzialania programu moga zajmowac coraz wiecej miejsca, az bedzie stack over flow.
Sam wyciek jest najcz�ciej wynikiem:
- zapominalstwa
- wielu �cie�ek powrotu z funkcji
- przypisania nowej warto�ci do wska�nika przed wywo�aniem delete
- niezwolnienia element�w struktury po zwolnieniu struktury
- nie�wiadomo�ci, �e funkcja wywo�ywana alokuje pami��, kt�r� funkcja wywo�uj�ca powinna zwolni�.

Do kontrolowania wyciekow przydadza sie takie programy jak:
Windows - Visual Leak Detector
Linux - valgrind
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	STACK OVERFLOW
Do przepe�nienia stosu dochodzi zazwyczaj, gdy:
- wywo�ywanych jest kaskadowo zbyt wiele funkcji (ka�da funkcja wywo�uje kolejn�);
- obszerne parametry do funkcji s� przekazywane bezpo�rednio (np. tablice jako warto�ci), a nie przez wska�niki;
- w funkcji deklarowane s� zmienne lokalne o du�ej obj�to�ci (np. tablice wielowymiarowe).