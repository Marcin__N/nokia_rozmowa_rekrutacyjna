#include <iostream>
#include <vld.h>

using namespace std;

void swapr(int &a, int &b)
{
	int temp = b;
	b = a;
	a = temp;
}

int refcube(const int & a)
{
	return a * a * a;
}

int main()
{
	/////////////////////////////////////////////////////////////////////////////////////////////////
			// ZMIENNA REFERENCYJNA JAKO ALIAS
	{

		cout << endl << endl;
		int a = 1;
		cout << ++a << " " << ++a << " " << ++a;

		int rats = 101;
		int & rodents = rats;
		cout << "Rats = " << rats << endl;
		cout << "Rodents = " << rodents << endl;
		rodents += 99;
		cout << "Rats = " << rats << endl;
		cout << "Rodents = " << rodents << endl;
		cout << "Adres rats = " << &rats << endl;
		cout << "Adres rodents = " << &rodents << endl;
		int cats = 10;
		rodents = cats;
		cout << "Teraz rodents to cats: " << rodents << endl;
	}

	cin.get();
	system("cls");

	/////////////////////////////////////////////////////////////////////////////////////////////////
			// ZMIENNA REFERENCYJNA JAKO PARAMETR FUNKCJI
	{
		/*
		Uzywajac referencji do struktur odowlujemy sie jak w klasach przez kropke '.'
		*/
		int one = 1;
		int two = 2;
		swapr(one, two);
		//swapr(one, two + 3);								// niedozwolone, my przekazujemy referencje, a nie wartosc
		cout << "One = " << one << endl;
		cout << "Two = " << two << endl;
		cout << "Funkcje refcube returnuje wartosc: "
			<< refcube(one) <<								// funkcja ma stala referencje wiec nie zmienia wartosci one
			", a zmienna one wynosi: " << one << endl;
	}

	cin.get();
	system("cls");

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
			// WSKAZNIKI I MODYFIKATOR CONST
	{
		int age = 39;
		const int * pt = &age;				// pt wskazuje wartosc const int
		//*pt += 1;							// nie mozna bo pt wskazuje STALA wartosc
		//cin >> *pt;						// j.w.
		//*pt = 20;							// j.j.
		age = 20;							// mozliwe, bo wskaznik jest na STALA, a zmienna nie musi byc stala (subtelnosc)

		const int number = 10;
		const int * ptr_nr = &number;		// dozwolone bo NIE da sie uzywajac wskaznika zmienic stalej
		//int * no_ptr_nr = &number;		// niedozwolone, bo przez wskaznik moglibysmy zmienic stala
	}
	{
		int age = 39;
		int number = 30;
		const int * pt = &age;				// wskaznik na stala
		int * const finger = &age;			// staly wskaznik na zmienna
		*finger = 20;						// mozna, bo to wskaznik jest staly, a nie zmienna
		//finger = &number;					// nie mozna, bo wskaznik jest staly
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
			// ARYTMETYKA WSKAZNIKOW
	{
		int tab_2_wym[3][4] = { { 1, 2, 3, 4 },{ 4, 5, 6, 7 },{ 8, 9, 10, 11 } };
		cout << "Prawda jest, ze: " << (tab_2_wym[2][3] == *(*(tab_2_wym + 2) + 3));
		/*
		tab_2_wym				to wskaznik na pierwszy element
		tab_2_wym + 2			to wskaznik 2 wiersza
		*(tab_2_wym + 2)		wiersz 2, tablica 4 liczb int, czyli nazwa tablicy
		czyli wskaznik pierwszej liczby int w wierszy, czyli tab_2_wym[2]
		*(tab_2_wym + 2) + 3	wskaznik 3 liczby int w 2 wierszu, czyli ar[2]+3
		*/
	}

	cin.get();
	system("cls");

	/////////////////////////////////////////////////////////////////////////////////////////////////
			// WSKAZNIKI NA FUNKCJE

	// bedziemy poslugiwac sie funkcja swapr i refcube z przykladow odnosnie referencji
	{
		int a = 2;
		int(*ptr)(const int &);		// wskaznik ptr wskazuje na funkcje zracajaca int i posiadajaca jeden argument const int &
									// aby w latwy sposob tworzyc wskazniki na funkcje zamieniamy w prototypie nawet funkcji na (*wskaznik)
		ptr = refcube;				// nazwa funkcji to jej adres, przypisujemy wskaznikowi ptr adres funkcji refcube
		//ptr = swapr;				// musi zgadzac sie typ zwracany jak i argumenty
		cout << "Sposob uzycia funki za pomoca wskaznika, wynik: " << (*ptr)(a) << endl;
	}

	cin.get();
	system("cls");

	/////////////////////////////////////////////////////////////////////////////////////////////////
			// WSKAZNIKI THIS

	/////////////////////////////////////////////////////////////////////////////////////////////////
			// WSKAZNIKI THIS

	


	system("cls");
	return 0;
}