#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
using namespace std;

int main()
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// AUTO
	{
		int x = 10;
		int* ptr = &x;
		auto auto_val = *ptr;						// auto samo wie jaki typ zmiennej bedzie
		cout << x << " == " << auto_val << endl;	// w tym przypadku jest to int

		initializer_list<double> il;
		for (initializer_list<double>::iterator p = il.begin(); p != il.end(); p++)		// zapis uzywajacy oelnego nazewnictwa
			;
		for (auto p = il.begin(); p != il.end(); p++)									// autodedukcja typu
			;
	}
	cin.get();
	system("cls");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// DECLTYPE
	{
		int x = 5;
		double y = 2.0;
		decltype(x*y) q;		// decltype okresla typ na podstawie typu zwracanego wyrazenia w ()
		q = y*y*x;				// tutaj jest to typ double
		cout << q << endl;
	}
	cin.get();
	system("cls");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// ZAKRESOWE PETLE FOR
	{
		double prices[5]{ 4.99, 3.50, 2.14, 0.99, 8.49 };		// lista inicjalizacyna z C++11
		for (double x : prices)									// (typ nazwa_dla_fora : nazwa_pojemnika)
			cout << x << " ";									// x bedzie co obrot petli kolejnym elementem w tabeli prices, az do ostatniego
		cout << endl;
		vector<int> vi(6);
		for (auto & x : vi)										// aby mozna bylo modyfikowa elementy zakresu typ musi miec referencje
		{
			x = rand()%25;
			cout << x << " ";
		}
	}
	cin.get();
	system("cls");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// REFERENCJA R-VALUE
	{
		int x = 10;
		int y = 23;
		int && r1 = 13;													// referencja r-value to tak naprawde referencja do zmiennych, 
		int && r2 = x + y;												// ktore wczesniej byly nie modyfikowalne bo nie byly zmiennymi
		double && r3 = sqrt(2.0);
		cout << "Wartosc/adres r1: " << r1 << " / " << &r1 << endl;		// aczkolwiek po referencji r-value "zdobyly" one swoj
		cout << "Wartosc/adres r1: " << r2 << " / " << &r2 << endl;		// wlasny adres w pamieci
		cout << "Wartosc/adres r1: " << r3 << " / " << &r3 << endl;
		/*
		Referencje r-value uzywa sie glownie do semantyki przenoszenia (konstruktor przenoszacy, operator przenoszenia). Wiecej o tym w 4_
		W skrocie pozwala to do przeniesienia elementow w czasie szybszym niz bysmy kopiowali je.
		*/
	}
	cin.get();
	system("cls");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LAMBDA
	{
		vector<int> numbers(100);
		srand(time(NULL));
		generate(numbers.begin(), numbers.end(), rand);						// funkcja z <algoritmh> w latwy sposob wypelniajaca wektor

		int count3 = 0;
		int count13 = 0;
		for_each(numbers.begin(), numbers.end(),							// funkcja iterujaca od-do i wykonujaca zadanie dla kazdego elementu
			[&](int x) {count3 += x % 3==0; count13 += x % 13==0; });		// lambda, dokladny opis znajduje sie w notatniku TEORIA
		cout << "Count3: " << count3 << endl;
		cout << "Count13: " << count13 << endl;
	}



	cin.get();
	return 0;
}