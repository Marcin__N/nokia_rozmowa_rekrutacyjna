#include <iostream>
#include <string>
#include <vector>
#include <memory>			// tutaj znajduja sie inteligentne wskazniki

using namespace std;

				// DLA SHARED_PTR I WEAK_PTR
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Person
{
public:
	string name;
	shared_ptr<Person> mother;
	shared_ptr<Person> father;
	vector<weak_ptr<Person>> kids;		// cykliczne odwolania spowodowaloby problemy (nie uuniecie nigdy obiektow), dlatego weak_ptr

	Person(const string& n, shared_ptr<Person> m = nullptr, shared_ptr<Person> f = nullptr) :name(n), mother(m), father(f) {}
	~Person() { cout << "Usuwam " << name << endl; }
};

shared_ptr<Person> initFamily(const string& name)
{
	shared_ptr<Person> mom(new Person("mama " + name));
	shared_ptr<Person> dad(new Person("tata " + name));
	shared_ptr<Person> kid(new Person(name, mom, dad));
	mom->kids.push_back(kid);
	dad->kids.push_back(kid);
	return kid;
}

int main()
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// PROSTE DZIALANIE WSKAZNIKA SHARED_PTR
	{
		//	deklaracja wskaznikow na dwa imiona
		shared_ptr<string> pNico(new string("nico"));
		shared_ptr<string> pJutta(new string("jutta"));
		// dzialanie na wskaznikach (zmiana pierwszej ltiery na duza)
		(*pNico)[0] = 'N';
		pJutta->replace(0, 1, "J");
		// umieszczanie wskaznikow w kontenerze vector
		vector<shared_ptr<string>> whoMadeCoffe;
		whoMadeCoffe.push_back(pJutta);
		whoMadeCoffe.push_back(pJutta);
		whoMadeCoffe.push_back(pNico);
		whoMadeCoffe.push_back(pJutta);
		whoMadeCoffe.push_back(pNico);
		// wypisanie element�w z kontenera
		for (auto ptr : whoMadeCoffe)
		{
			cout << *ptr << " ";
		}
		cout << endl;

		// ponownie zmienienie pisowni jednego z imion
		*pNico = "Nicolai";
		// oraz wypisz wszystkie elementy
		for (auto ptr : whoMadeCoffe)
		{
			cout << *ptr << " ";
		}
		cout << endl;
		// funkcja ta pokazuje ile jest aktualnie wskaznikow na ten typ (3 w vectorze i 1 poza)
		cout << "use_count: " << whoMadeCoffe[0].use_count() << endl;
	}
	cin.get();
	system("cls");
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// WEAK_PTR
	{
		shared_ptr<Person> p = initFamily("nico");			// tworzymy rodzine z nico oraz wskaznik na ta rodzine
		cout << "Rodzina nico istnieje" << endl;
		cout << "- nico jest wspoldzielony " <<
			p.use_count() << "-krotnie" << endl;			// jest tylko 1 wskaznik wlasnosciowy nico, bo reszta weak_ptr
		cout << "- imie pierwszego dziecka mamy nico: "
			<< p->mother->kids[0].lock()->name << endl;		// bo weak nie ma * i -> to lock() zwraca obiekt wskaznika wspoldzielonego
															// gdyby ostatni obiekt  byl uuniey wtedy zwroci pusty wskaznik wspoldzielony
		p = initFamily("jim");								// usuwa rodzine nico, bo tworzy tam rodzina jima
		cout << "rodzina jima istnieje" << endl;

		/*
		Gdybysmy nie uzyli weak_ptr w initFamily to spowodowaloby to cykliczne odwolania, ktore powoduja ze zawsze jest conajmniej
		1 wskaznik na obiekt, wiec nigdy by nie usunieto rodziny nico.
		*/
	}
	cin.get();
	system("cls");
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// UNIQUE_PTR
	{
		unique_ptr<string> pNico1(new string("nico"));
		cout << "pNico1: " << *pNico1 << "\t" << pNico1.get() << endl;
		//unique_ptr<string> pNico2 = pNico1.release();						// nie mozliwe, z powodu funkcji usuwajacej
		//string * ptr = pNico1.release();									// a tutaj juz ptr nie posiada funkcji usuwajacej wiec brak konwersji
		unique_ptr<string> pNico2 = move(pNico1);
		cout << "Po przekazaniu: " << endl;
		cout << "pNico1: " << "\t" << pNico1.get() << endl;
		cout << "pNico2: " << *pNico2 << "\t" << pNico2.get() << endl;

		unique_ptr<string[]> up_tab(new string[10]);
		//*up_tab = "Wersja tablicowa nie definiuje operatora * ";
		up_tab[0] = "Natomiast istnieje operator indeksowania";
		cout << endl << up_tab[0] << endl;
	}

	cin.get();
	return 0;
}